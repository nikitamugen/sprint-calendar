import { Dates } from "./dates.js";

/// Класс спринта
///
export class Sprint {
    constructor(number, month) {
        this._number = number;
        this._month = month;
        this._dates = [];
        this._workHours = 0;
        this.firstWeekNumber = 0;
        this.lastWeekNumber = 0;
    }

    createNextSprint(month) {
        if (this._number === undefined) {
            return new Sprint(1, month);
        }
        return new Sprint(this._number + 1, month);
    }

    getWorkHours() {
        return this._workHours;
    }

    addDate(date) {
        this._dates.push(date);
        const weekNumber = Dates.getWeekNumber(date);
        if (this.firstWeekNumber === 0) {
            this.firstWeekNumber = weekNumber;
        } else {
            this.firstWeekNumber = Math.min(this.firstWeekNumber,weekNumber);
        }
        this.lastWeekNumber = Math.max(this.lastWeekNumber,weekNumber);
    }

    getDateTimes() {
        const dateTimes = this._dates.map(d => d.getTime());
        return dateTimes;
    }

    addWorkHours(hours) {
        this._workHours = this._workHours + hours;
    }

    getNumber() {
        return this._number;
    }

    getMonth() {
        return this._month;
    }

    getSprintClassName() {
        return 'sprint-' + this.getNumber();
    }
}