import { Arrays } from "./arrays.js";
import { Dates } from "./dates.js";
import { Sprint } from "./sprint.js";

export class SprintService {
    constructor(config) {
        this._config = config;
    }

    static get csvPath() {
        return "csv/production-calendar.csv";
    }

    parseYearCsvDataWeekends(csvData, year) {
        const yearKeyName = "Год/Месяц";
        const yearString = year.toString();
        return Arrays.flatMap(
            csvData.filter(yearData => yearData[yearKeyName] === yearString),
            yearData => {
                return Arrays.flatMap(
                        Dates.MONTH_LOCALE_NAMES,
                        (monthName, monthIndex) => {
                            const monthCsvData = yearData[monthName];
                            return this.parseMonthCsvDataWeeks(monthCsvData, year, monthIndex);
                        });
            });
    }

    parseMonthCsvDataWeeks(monthCsvData, year, monthIndex) {
        const monthCsvDataArray = monthCsvData.split(",");
        const firstMonthDate = new Date(year, monthIndex, 1);
        const workDateData = Dates.getMonthDatesRange(firstMonthDate).reduce((acc, date) => {
            const workDateObject = this.getWorkDayObjectByCsvDatesArray(date, monthCsvDataArray);
            if (workDateObject !== undefined) {
                acc.push(workDateObject);
            }
            return acc;
        }, [])

        return this.getWeeksData(workDateData);
    }

    getWorkDayObjectByCsvDatesArray(date, csvDatesArray) {
        const dateString = date.getDate().toString();
        const isDateInCsvData = (csvDatesArray.indexOf(dateString) >= 0);
        const isWeekend = Dates.isWeekend(date);

        if (isDateInCsvData && !isWeekend) {
            return undefined;
        }

        const prePartyDateString = dateString+"*";
        if (csvDatesArray.indexOf(prePartyDateString) > 0) {
            return {date: date, hours: 7};
        }

        if (isWeekend) {
            return undefined;
        }

        return {date: date, hours: 8};
    }

    getWeeksData(workDatesData) {
        return workDatesData.reduce((acc, workDateData) => {
            const weekNumber = Dates.getWeekNumber(workDateData.date);
            let weekData = acc[weekNumber];
            if (weekData === undefined) {
                weekData = {
                    month: workDateData.date.getMonth(),
                    datesCount: 0,
                    isMonthEnd: false,
                    dates: []
                };
            }

            weekData.datesCount = ++weekData.datesCount;
            weekData.dates.push(workDateData);

            if (Dates.isDateInLastWeekOfMonth(workDateData.date)) {
                weekData.isMonthEnd = true;
            }

            acc[weekNumber] = weekData;
            return acc;
        }, {});
    }

    getSprintsData(weeksData) {
        let sprint = new Sprint();
        return weeksData.reduce((acc, oneMonthWeeksData) => {
           Object.values(oneMonthWeeksData).forEach(weekData => {
               if (!weekData.isMonthEnd || weekData.datesCount >= 5) {
                   sprint = sprint.createNextSprint(weekData.month);
                   acc.push(sprint);
               }

               weekData.dates.forEach(dateData => {
                   sprint.addDate(dateData.date);
                   sprint.addWorkHours(dateData.hours);
               })
           });
           return acc;
        }, []);
    }

    retrieveSprintDataByYear(year) {
        return new Promise((resolve, reject) => {
            d3.csv(SprintService.csvPath)
                .then(csvData => {
                    const yearData = this.parseYearCsvDataWeekends(csvData, year);
                    const sprintDatesData = this.getSprintsData(yearData);
                    resolve(sprintDatesData);
                })
                .catch(error => reject(error));
        });
    }
}