import { Sprints } from "./sprints.js";
import { Dates } from "./dates.js";
import { Calendars } from "./calendars.js";
import { SprintService } from "./sprint-service.js";
import { CalendarConfig } from "./calendar-config.js";
import { MonthLineConfig } from "./month-line-config.js";

// CALENDAR
//
export class Calendar {
    constructor(config) {
        this._config = config;
    }

    refresh() {
        this.renderCalendar(this._config);

        Sprints.clearWorkHours();

        const sprintService = new SprintService(this._config);
        sprintService.retrieveSprintDataByYear(this._config.getYear())
            .then(sprintData => {
                this.renderSprints(sprintData, this._config);
                Calendars.toggleCurrentSprintActive(this._config);
                Calendars.scrollToCurrentMonth(this._config);
            })
    }

    renderCalendar(calendarConfig) {
        d3.select("div.year").text(calendarConfig.getYear());

        Calendars.clearAll();

        var svg = d3.select("div.calendar")
            .selectAll("svg")
            .data([calendarConfig.getYear()])
            .enter()
            .append("svg").attr("class", "calendar-svg")
            .attr("width", calendarConfig.getWidth())
            .attr("height", calendarConfig.getHeight())
            .on('click', function() {
                if (d3.event.target === svg.node()) {
                    d3.selectAll('.sprint-active')
                    .each(function() {
                        Sprints.checkActive(this);
                    })
                }
        })

        let monthLineConfig = MonthLineConfig.createFirstLineConfigByCalendarConfig(calendarConfig);
        this.createMonthLine(calendarConfig, monthLineConfig);
        while (monthLineConfig.canGetNext()) {
            monthLineConfig = monthLineConfig.getNext();
            this.createMonthLine(calendarConfig, monthLineConfig);
        }

        //this._appendSprints();
    };

    createMonthLine(calendarConfig, monthLineConfig) {
        const svg = d3.select(".calendar-svg");

        // MONTH LINE
        //
        const monthLine = svg
            .append("g").attr("class", "month-line")
            .selectAll("rect")
            .data(d3.timeMonths(monthLineConfig.firstDate, monthLineConfig.lastDate));

        monthLine
            .enter()
            .each(function (d, i) {
                const month = d3.select(this)
                    .append("g")
                    .attr("class", "month")
                    .attr("name", "month-"+Dates.getMonthNameByMonth(d.getMonth()))
                    .attr("y", monthLineConfig.getTopY());

                // MONTH TITLE
                //
                month.append("text")
                    .attr("class", "month-text")
                    .attr("name", "month-name-"+Dates.getMonthNameByMonth(d.getMonth()))
                    .style('cursor', 'pointer')
                    .attr("text-anchor", "middle")
                    .attr("x", d => monthLineConfig.getMonthTitleX(d))
                    .attr("y", d => monthLineConfig.getMonthTitleY(d))
                    .text(d => Dates.MONTH_LOCALE_NAMES[d.getMonth()])
                    .datum(d => d.getMonth())
                    .on('click', function(d) {
                        d3.selectAll('.sprint')
                            .filter(sprint => {
                                return (sprint.getMonth() === d)
                            })
                            .each(function() {
                                Sprints.checkActive(this);
                            })
                    });

                // DAYS DATA
                //
                const monthNode = month
                    .selectAll("rect")
                    .data(Dates.getMonthDatesRange(d));

                const day = monthNode.enter()
                    .append("g").attr("class", "day");

                // DAY RECT
                //
                day.append("rect").attr("class", "day-rect")
                    .attr("fill", "none")
                    .attr("width", calendarConfig.getCellSize())
                    .attr("height", calendarConfig.getCellSize())
                    .attr("x", d => monthLineConfig.getDayX(d))
                    .attr("y", d => monthLineConfig.getDayY(d));

                // DAYS TEXT
                //
                day.append("text").attr("class", "day-text")
                    .attr("fill", "none")
                    .attr("width", calendarConfig.getCellSize())
                    .attr("height", calendarConfig.getCellSize())
                    .attr("x", d => monthLineConfig.getDayX(d) + 14)
                    .attr("y", d => monthLineConfig.getDayY(d) + 18)
                    .attr("text-anchor", "middle")
                    .text(d => d.getDate());

                // MONTH PATH LINES
                //
                month.append("path")
                    .attr("class", "month-paths")
                    .attr("d", Calendars.createPathMonth(d, calendarConfig, monthLineConfig));
            });
    }

    renderSprints(sprintData, calendarConfig) {
        sprintData.forEach(sprint => this.renderSprint(sprint, calendarConfig))
    }

    renderSprint(sprint, calendarConfig) {
        const sprintDateTimes = sprint.getDateTimes();
        d3.selectAll('.month')
            .filter(d => {
                return d.getMonth() === sprint.getMonth()
            })
            .each(function(monthNode) {
                const monthItem = d3.select(this);
                let sprintPath = "";
                let weekMap = {};
                monthItem
                    .selectAll(".day-rect")
                    .filter(d => {
                        return sprintDateTimes.includes(d.getTime());
                    })
                    .each(function(d) {
                        const x = parseInt(d3.select(this).attr("x"));
                        const y = parseInt(d3.select(this).attr("y"));
                        const width = calendarConfig.getCellSize();
                        const height = calendarConfig.getCellSize();

                        const weekNumber = Dates.getWeekNumber(d);
                        if (weekMap.hasOwnProperty(weekNumber)) {
                            weekMap[weekNumber].width = width;
                        } else {
                            weekMap[weekNumber] = {width: width};
                        }
                        weekMap[weekNumber].height = height*(Dates.getWeekDay(d)+1);
                        weekMap[weekNumber].x = Math.min(x, weekMap[weekNumber].x);
                        if (isNaN(weekMap[weekNumber].x)) {
                            weekMap[weekNumber].x = x;
                        }

                        let path = "M" + x + "," + y
                            + "V" + (y + height)
                            + "H" + (x + width)
                            + "V" + y
                            + "Z";

                        sprintPath += path;
                    });

                let sprintHeight = 0;
                let sprintWidth = 0;
                let sprintX;
                for (let weekNumber in weekMap) {
                    const weekItem = weekMap[weekNumber];

                    sprintHeight = Math.max(sprintHeight, weekItem.height);
                    sprintWidth += weekItem.width;

                    sprintX = Math.min(sprintX, weekItem.x);
                    if (isNaN(sprintX)) {
                        sprintX = weekItem.x;
                    }

                    sprintPath += "M" + (weekItem.x) +
                        "," + (monthItem.attr("y"))
                    sprintPath += "L" + (weekItem.x + calendarConfig.getSprintNumberAngleOffset()) +
                        "," + (monthItem.attr("y") - calendarConfig.getSprintNumberHeight())
                    sprintPath += "L" + (weekItem.x + calendarConfig.getSprintNumberAngleOffset() + calendarConfig.getCellSize()) +
                        "," + (monthItem.attr("y") - calendarConfig.getSprintNumberHeight())
                    sprintPath += "L" + (weekItem.x + calendarConfig.getCellSize()) +
                        "," + (monthItem.attr("y"));
                    sprintPath += "L" + (weekItem.x) +
                        "," + (monthItem.attr("y"));
                }

                const sprintColor = Sprints.getColorBySprintNumber(sprint.getNumber());
                monthItem
                    .insert("path",":first-child")
                    .attr("class", "sprint sprint-"+sprint.getNumber())
                    .on('click', function() {
                        Sprints.checkActive(this);
                    })
                    .on('mouseover', function() {
                        if (d3.event.shiftKey) {
                            Sprints.checkActive(this);
                        }
                    })
                    .attr("d", sprintPath)
                    .style("fill", sprintColor)
                    .attr("opacity", 0.7)
                    .attr("height", sprintHeight)
                    .attr("width", sprintWidth)
                    .attr("y", monthItem.attr("y"))
                    .attr("x", sprintX)
                    .datum(sprint);

            
            const sprintNumberX = sprintX + (sprintWidth + calendarConfig.getSprintNumberAngleOffset()) / 2;
            const sprintNumberY = monthItem.attr("y") - (calendarConfig.getSprintNumberHeight() - 10) / 2;
            const sprintCircleColor = d3.color(sprintColor).darker(2.2);
            monthItem
                .append("circle").attr("class", "sprint-circle")
                .style("fill", sprintCircleColor)
                .attr("text-anchor", "middle")
                .attr("cx", sprintNumberX)
                .attr("cy", sprintNumberY-6)
                .attr("r", 14)
                .text(sprint.getNumber());

            const sprintNumberColor = d3.color(sprintColor).brighter(0.9);
            monthItem
                .append("text").attr("class", "sprint-text")
                .style("fill", sprintNumberColor)
                .attr("text-anchor", "middle")
                .attr("x", sprintNumberX)
                .attr("y", sprintNumberY)
                .text(sprint.getNumber());
        })

            
    }
}

window.onload = function () {
    let calendarConfig = new CalendarConfig();
    calendarConfig.setYear(Dates.getCurrentYear());

    let calendar = new Calendar(calendarConfig);
    calendar.refresh();

    d3.select('div.prev')
        .on('click', function () {
            calendarConfig.setYear(calendarConfig.getYear() - 1);
            calendar.refresh();
        });

    d3.select('div.next')
        .on('click', function () {
            calendarConfig.setYear(calendarConfig.getYear() + 1);
            calendar.refresh();
        });
};