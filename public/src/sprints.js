const COLORS = [
    "#FFAC9A",
    "#FFCF9A",
    "#98DBF5",
    "#96F8BB",
    "#B9A0F7",
]

/// Утилиты для работы со спринтами
///
export class Sprints {
    static checkActive(sprintDomElement) {
        const sprintItem = d3.select(sprintDomElement);
        sprintItem.classed('sprint-active', !sprintItem.classed('sprint-active'));

        const isActive = sprintItem.classed('sprint-active');
        if (isActive) {
            this.createWorkHoursPopup(sprintDomElement);
        } else {
            this.removeSprintWorkHoursPopup(sprintDomElement);
        }

        this.refreshWorkHours();
    }

    static clearWorkHours() {
        d3.select('.workHours')
            .text("");
    }

    static refreshWorkHours() {
        let totalWorkHours = 0;
        d3.selectAll(".sprint-active")
            .each(function(sprint) {
                totalWorkHours += sprint.getWorkHours();
            });

        let workHoursText = "";
        if (totalWorkHours > 0) {
            workHoursText = "Всего рабочих часов: "+totalWorkHours;
        }

        d3.select('.workHours')
            .text(workHoursText);
    }

    static getSprintClass(sprint) {
        return 'sprint-' + sprint.getNumber();
    }

    static getWorkHoursPopupClass(sprint) {
        return 'workHours-sprint' + sprint.getNumber();
    }

    static createWorkHoursPopup(sprintDomElement) {
        const sprintItem = d3.select(sprintDomElement);
        const x = parseInt(sprintItem.attr('x')) + 1;
        
        const y = parseInt(sprintItem.attr('y')) +
            parseInt(sprintItem.attr('height')) +
            10;
        
        const width = parseInt(sprintItem.attr('width'));
        const height = 120;

        const svg = d3.select('div.calendar').select('svg');

        const popupPath = "M" + x + "," + y
            + "V" + (y + height)
            + "H" + (x + width)
            + "V" + y
            + "L" + (x + width/2) + "," + (y - 15)
            + "Z";
        const popup = svg
            .append("path")
            .attr("class", "sprintWorkHoursPopup")
            .attr("width", width)
            .attr("height", height)
            .attr("d", popupPath);

        const sprintData = sprintItem.data()[0];
        const workHours = sprintData.getWorkHours();

        const popupText = svg
            .append("text")
            .attr("class", "sprintWorkHoursText")
            .attr("text-anchor", "middle")
            .attr("transform", d => {
                return "translate("
                    +(+x+width/2+9)+","
                    +(+y+height/2)+")rotate(-90)";
            })
            .text(workHours+" часов");

        popup.classed(this.getWorkHoursPopupClass(sprintData), true);
        popupText.classed(this.getWorkHoursPopupClass(sprintData), true);
    }

    static removeSprintWorkHoursPopup(sprintDomElement) {
        const sprintItem = d3.select(sprintDomElement);
        const sprintData = sprintItem.data()[0];
        d3.selectAll(`.${this.getWorkHoursPopupClass(sprintData)}`)
            .remove();
    }

    static getColorBySprintNumber(sprint) {
        const diff = Math.trunc(sprint / COLORS.length);
        const colorIndex = sprint - (COLORS.length * diff);

        return COLORS[colorIndex];
    };
}