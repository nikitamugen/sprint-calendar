/// Утилиты для работы с датами
///
export class Dates {
    static get MONDAY() {
        return 1;
    }

    static get FRIDAY() {
        return 5;
    }

    static get SUNDAY() {
        return 0;
    }

    static get SATURDAY() {
        return 6;
    }

    static isWeekend(date) {
        const weekDay = date.getDay();
        return (weekDay === Dates.SATURDAY || weekDay === Dates.SUNDAY)
    }

    static get MONTH_NAMES() {
        return ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];
    }

    static getMonthNameByMonth(month) {
        return Dates.MONTH_NAMES[month];
    }

    static get MONTH_LOCALE_NAMES() {
        return ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
    }

    static getLocaleMonthNameByMonth(month) {
        return Dates.MONTH_LOCALE_NAMES[month];
    }

    static get MONTH_SHORT_NAMES() {
        return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }

    static getMonthShortNameByMonth(month) {
        return Dates.MONTH_SHORT_NAMES[month];
    }

    static getWeekDay(d) {
        // Отнять 1 т.к. предполагается,
        // что неделя начинается с понедельника,
        // а не с воскресенья
        //
        const dWeekDay = (d.getDay() - 1);

        // При этом если текущий день - воскресенье.
        // Тогда полученное число будет (-1)
        //
        if (dWeekDay < 0) {
            return this.SATURDAY;
        }
        return dWeekDay;
    }

    static addDays(d, count) {
        var newDate = new Date(d);
        newDate.setDate(d.getDate() + count);
        return newDate;
    }

    static getCurrentDate() {
        const currentDate = new Date();
        return currentDate;
    }

    static getCurrentYear() {
        return this.getCurrentDate().getFullYear();
    }

    static getCurrentMonth() {
        return this.getCurrentDate().getMonth();
    }

    static getCurrentWeekNumber() {
        const currentDate = Dates.getCurrentDate();
        return Dates.getWeekNumber(currentDate);
    }

    static getMinDate(dates) {
        const minDate = new Date(Math.min.apply(null,dates));
        return minDate;
    }

    static getFirstMonthDate(d) {
        return new Date(d.getFullYear(), d.getMonth(), 1);
    }

    static getLastMonthDate(d) {
        return new Date(d.getFullYear(), d.getMonth()+1, 0);
    }

    static getMonthDatesRange(d) {
        const firstDate = Dates.getFirstMonthDate(d);
        const lastDate = Dates.addDays(Dates.getLastMonthDate(d),1)
        return d3.timeDays(firstDate, lastDate);
    }

    static getFirstDayOfYear(d) {
        return new Date(d.getFullYear(), 0, 1);
    }

    static getWeekNumber(d) {
        const firstDate = Dates.getFirstDayOfYear(d);
        return Math.ceil((((d - firstDate) / 86400000) + firstDate.getDay()) / 7);
    }

    static isDateInLastWeekOfMonth(d) {
        const lastMonthDate = Dates.getLastMonthDate(d);
        const lastMonthDateWeek = Dates.getWeekNumber(lastMonthDate);
        const thisDateWeek = Dates.getWeekNumber(d);
        return (lastMonthDateWeek === thisDateWeek);
    }
}