/// Настройки календаря
///
export class CalendarConfig {
    constructor() {
        this._year = undefined;
    }

    getYearLineTopOffset() {
        return 5;
    }

    getMonthLineLeftOffset() {
        return 15;
    }

    getMonthsPerLine() {
        return 4;
    }

    getMonthLines() {
        const monthsPerYear = 12;
        return (monthsPerYear / this.getMonthsPerLine());
    }

    getDayRowsPerMonthLine() {
        const daysPerWeek = 7;
        return (daysPerWeek * this.getMonthLines());
    }

    getAvgWeeksPerMonth() {
        return 5;
    }

    getCellSize() {
        return 45;
    }

    getSprintWorkHoursPopupHeight() {
        return 100;
    }

    getMonthsOffset() {
        return 80;
    }

    getWidth() {
        return (this.getAvgWeeksPerMonth() * this.getMonthsPerLine() + this.getMonthsOffset() /
            this.getCellSize() * (this.getMonthsPerLine() - 1)) * this.getCellSize();
    }

    getHeight() {
        return this.getYearLineTopOffset() +
        this.getDayRowsPerMonthLine() * this.getCellSize() +
        this.getMonthLines() * this.getMonthsOffset() +
        ((this.getSprintNumberHeight() + this.getSprintNumberTopOffset())*3);
    }

    getSprintNumberHeight() {
        return 32;
    }
    getSprintNumberTopOffset() {
        return 10;
    }
    getSprintNumberAngleOffset() {
        return 25;
    }

    getWeekEndColor() {
        return "#ffef96";
    }
    getTimeFormat() {
        return d3.timeFormat("%Y%m%d");
    }
    getTimeWeekFormat() {
        return d3.timeFormat("%W");
    }
    getFormatPercent() {
        return d3.format(".1%");
    }

    getYear() {
        return this._year;
    }
    setYear(year) {
        this._year = year;
    }
}