import { Dates } from "./dates.js";
import { Sprints } from "./sprints.js";

/// Утилиты календаря
///
export class Calendars {
    static clearAll() {
        d3.selectAll("svg").remove();
    }

    static toggleCurrentSprintActive(config) {
        if (config.getYear() != Dates.getCurrentYear()) {
            return;
        }

        let currentWeek = Dates.getCurrentWeekNumber();
        d3.selectAll('.sprint')
            .filter(function(sprintData) {
                const isCurrentSprint = (
                    (currentWeek >= sprintData.firstWeekNumber) &&
                    (currentWeek <= sprintData.lastWeekNumber));
                return isCurrentSprint;
            })
            .each(function() { 
                Sprints.checkActive(this);
            });
    }

    static scrollToCurrentMonth(config) {
        if (config.getYear() != Dates.getCurrentYear()) {
            return;
        }

        d3.selectAll('.month-text')
            .filter(function(monthNumber) {
                return (monthNumber === Dates.getCurrentMonth());
            })
            .each(function() {
                Calendars.scrollToElement(this)
            });
    }

    static scrollToElement(element, duration = 400) {
        const offsetTop = (window.pageYOffset || document.documentElement.scrollTop);
        const menuHeight = d3.select('#menu').node().getBoundingClientRect().height;

        d3.transition()
            .duration(duration)
            .tween("scroll", (offset => () => {
                var i = d3.interpolateNumber(offsetTop, offset);
                return t => scrollTo(0, i(t))
            })(offsetTop - menuHeight + element.getBoundingClientRect().top));
    }

    static createPathMonth(t0, calendarConfig, monthLineConfig) {
        var firstTimeWeek = d3.timeMonday.count(d3.timeYear(monthLineConfig.firstDate), monthLineConfig.firstDate);
        var t1 = new Date(calendarConfig.getYear(), t0.getMonth() + 1, 0),
            d0 = Dates.getWeekDay(t0),
            w0 = d3.timeMonday.count(d3.timeYear(t0), t0) - firstTimeWeek,
            d1 = Dates.getWeekDay(t1),
            w1 = d3.timeMonday.count(d3.timeYear(t1), t1) - firstTimeWeek;

        var monthOffset = (t0.getMonth() - monthLineConfig.firstDate.getMonth()) * calendarConfig.getMonthsOffset();
        var leftOffset = calendarConfig.getMonthLineLeftOffset() + monthOffset,
            topOffset = monthLineConfig.getTopY();

        let path = "M" + ((w0 + 1) * calendarConfig.getCellSize() + leftOffset)
            + "," + (d0 * calendarConfig.getCellSize() + topOffset)
            + "H" + (w0 * calendarConfig.getCellSize() + leftOffset)
            + "V" + (7 * calendarConfig.getCellSize() + topOffset)

            + "H" + (w1 * calendarConfig.getCellSize() + leftOffset)
            + "V" + ((d1 + 1) * calendarConfig.getCellSize() + topOffset)

            + "H" + ((w1 + 1) * calendarConfig.getCellSize() + leftOffset)
            + "V" + topOffset

            + "H" + ((w0 + 1) * calendarConfig.getCellSize() + leftOffset)
            + "Z";

        return path;
    }
}