export class Arrays {
    static flatMap(array,lambda) {
        return Array.prototype.concat.apply([], array.map(lambda));
    };
}