import { Dates } from "./dates.js";

export class MonthLineConfig {
    static createFirstLineConfigByCalendarConfig(calendarConfig) {
        const firstDate = new Date(calendarConfig.getYear(), 0, 1);
        const lastDate = new Date(calendarConfig.getYear(), calendarConfig.getMonthsPerLine(), 1);
        const monthLineConfig = new MonthLineConfig(firstDate, lastDate, calendarConfig);

        return monthLineConfig;
    }

    constructor(firstDate, lastDate, calendarConfig) {
        this._firstDate = firstDate;
        this._lastDate = lastDate;
        this._calendarConfig = calendarConfig;
    }

    get firstDate() {
        return this._firstDate;
    }

    get lastDate() {
        return this._lastDate;
    }

    get calendarConfig() {
        return this._calendarConfig;
    }

    getNext() {
        if (!this.canGetNext()) {
            throw 'Нельзя получить конфигурации для следующего ряда. Все месяцы уже были перечислены.';
        }
        const firstDate = new Date(this.calendarConfig.getYear(), this.lastDate.getMonth(), 1);
        const lastDate = new Date(this.calendarConfig.getYear(), this.lastDate.getMonth() + this.calendarConfig.getMonthsPerLine(), 1);
        const nextConfig = new MonthLineConfig(firstDate, lastDate, this.calendarConfig);

        return nextConfig;
    }

    canGetNext() {
        const decemberIndex = 0;
        return (this.lastDate.getMonth() !== decemberIndex);
    }

    getTopY() {
        const firstLineOffset = 20;
        const headerLineHeight = 40;
        const weekDayCount = 7;

        return (this.getCurrentLineIndex() * (weekDayCount * this.calendarConfig.getCellSize() + headerLineHeight)) +
            firstLineOffset + this.calendarConfig.getYearLineTopOffset() +
            (this.getCurrentLineIndex()+1) * (this.calendarConfig.getSprintNumberHeight() + this.calendarConfig.getSprintNumberTopOffset());
    }
    getCurrentLineIndex() {
        const month = this.firstDate.getMonth();
        return month / this.calendarConfig.getMonthsPerLine();
    }

    getDayX(d) {
        let dateOffsets = this.calculateDateOffsets(d);
        return (dateOffsets.timeWeek - dateOffsets.firstTimeWeek)
            * this.calendarConfig.getCellSize()
            + dateOffsets.monthOffset
            + this.calendarConfig.getMonthLineLeftOffset();
    };

    getDayY(d) {
        const dayOfWeek = Dates.getWeekDay(d);
        return this.getTopY() + dayOfWeek * this.calendarConfig.getCellSize();
    };

    getMonthTitleX(d) {
        return this.getDayX(d) + this.calendarConfig.getCellSize() * this.calendarConfig.getAvgWeeksPerMonth() / 2;
    }

    getMonthTitleY(d) {
        return this.getTopY() - (this.calendarConfig.getSprintNumberHeight() + this.calendarConfig.getSprintNumberTopOffset())
    }

    calculateDateOffsets(d) {
        let dateOffsets = {};
        dateOffsets.firstTimeWeek = d3.timeMonday.count(d3.timeYear(this.firstDate), this.firstDate);
        dateOffsets.timeWeek = d3.timeMonday.count(d3.timeYear(d), d);
        dateOffsets.monthOffset = (d.getMonth() - this.firstDate.getMonth()) * this.calendarConfig.getMonthsOffset();

        return dateOffsets;
    };
}